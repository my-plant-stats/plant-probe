var PouchDB = require('pouchdb');
require('dotenv').config({ path: '.env' })

var db = new PouchDB('plantStats');
// use custom db url herer via .env || "http://localhost:4000/plantStats"
// var remotePouch = process.env.REMOTE_DB || "http://localhost:4000/plantStats";
var remotePouch = "http://192.168.178.43:4000/plantStats";

db.setMaxListeners(25);

function addNewPlantStat(moisture, temperature, lighting) {
    var plantStat = {
        _id: new Date().toISOString(),
        moisture: moisture,
        temperature: temperature,
        lighting: lighting,
        humidity: humidity
    };

    db.put(plantStat, function callback(err, result) {
        if (!err) {
            console.log('Successfully added current plant stats.');
        }
    })
}

function createPlantStat(moisture, temperature, lighting, humidity) {
    var plantStat = {
        _id: new Date().toISOString(),
        moisture: moisture,
        temperature: temperature,
        lighting: lighting,
        humidity: humidity
    };
    return plantStat;
}

function addFinishedPlantStat(plantStat) {
    db.put(plantStat, function callback(err, result) {
        if (!err) {
            console.log('Successfully added current plant stats.');
            // db.close();
            return result
        } else {
            console.log(err);
        }
    })
}

function getPlantStat() {
    db.allDocs({ include_docs: true, descending: true }, function(err, doc) {
            // UI function for stats
            console.log(doc.rows);
            return doc.rows;
        })
        //closeDB();
}

function deleteAllStats() {
    db.allDocs({ include_docs: true, descending: true }, function(err, doc) {
        doc.rows._id.forEach(element => {
            db.remove(element);
        });
    })
}

function sync() {
    //syncDom.setAttribute('data-sync-state', 'syncing');
    console.log(remotePouch);
    try {
        var opts = { live: false, retry: false };
        db.replicate.to(remotePouch, opts);
        db.replicate.from(remotePouch, opts);
        console.log('Successfully synced plant stats.');

    } catch (error) {
        console.log(error);
    }
}


module.exports = { addNewPlantStat, addFinishedPlantStat, createPlantStat, getPlantStat, sync };