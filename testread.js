const ADS1015 = require('ads1015');
const i2c = require('i2c-bus');
const sensorLib = require('node-dht-sensor');
const sensor = require("node-dht-sensor").promises;
const intervall = 60000; // milsec

sensor.setMaxRetries(10);
sensor.initialize(22, 4);

readSensor();

function readSensor() {



    async function DHT() {
        try {
            const res = await sensor.read(22, 4);
            return res;
        } catch (err) {
            console.error("Failed to read sensor data:", err);
        }
    }


    var DHT_ = {
        sensors: [{
            name: "Indoor",
            type: 22,
            pin: 4
        }],
        read: function() {
            for (var sensor in this.sensors) {
                var readout = sensorLib.read(
                    this.sensors[sensor].type,
                    this.sensors[sensor].pin
                );
                return ([readout.temperature.toFixed(1),
                    readout.humidity.toFixed(1)
                ])
            }
        }
    };

    const date = new Date();
    console.log("hello im loggin this for you");
    i2c.openPromisified(1).then(async(bus) => {

        const ads1015 = ADS1015(bus)
            //ads1115.gain = 1

        // MOISTURE 

        const inAir = 985;
        const inWater = 410;
        let moisture = 0;
        //const moisture = ((await ads1015.measure('0+GND') / 1023.0) * 3.3) // A0
        const moistureSensorRead = await ads1015.measure('0+GND') // A0
        if (moistureSensorRead > inAir) {
            moisture = 0;
        } else if (moistureSensorRead < inWater) {
            moisture = 100;
        } else {
            const range = inAir - inWater;
            const scale = range / 100;
            moisture = 100 - ((moistureSensorRead - inWater) / scale);
        }

        // LIGHTING
        const VIN = 3.3;
        const R = 10000.0;
        //const lightingSensorRead = ((await ads1015.measure('1+GND') / 1023.0) * 3.3) // A1
        const lightingSensorRead = await ads1015.measure('1+GND') // A1
        const VOUT = lightingSensorRead * (VIN / 1023.0);
        const RLDR = (R * (VIN - VOUT)) / VOUT;
        const lighting = (500 * (1000 / RLDR)); // lux


        // TEMPERATURE AND HUMDITY
        let tempHumid = await DHT();
        tempHumid = await DHT();

        // BUILDING data
        var plantStat = {
            _id: date.toISOString(),
            moisture: moisture.toFixed(1),
            temperature: parseInt(tempHumid.temperature).toFixed(1),
            humidity: parseInt(tempHumid.humidity).toFixed(1),
            lighting: lighting.toFixed(1)
        };

        console.log(plantStat);
        return;
    })
}