# `plant-probe`

## `Hardware`

- `RaspberryPi` (Pi Zero works, installing the latest unofficial NODE Build 16.8.x)
- `ADS1015 12Bit I2C ADC` to use analog signals on the raspb
- `Capacitive Soil Moisture Sensor`  to measure the soil moisture value
- `LDR` to measure the lighting value
- `10Ω Resistor`
- `DHT22` Temperature and Humidity Sensor to measure the temperature and humidity values
- a bunch of wires

![](/images/plant-probe_wiring.png)

## `Usage`

setting up `.env` with `REMOTE_DB=your_url/plantStats` the default value is `"http://localhost:4000/plantStats"`
or writing the URL in database.js

running of nodejs, two scripts need to be run for:
    `sensors.js` / `sensors.sh` reading the current values 
    `syncdb.js` / `syncdb.sh` just syncing to remobe db (pouchdb-server)
    (`testread.js` / `testread.sh` sensor test read with console log)

Using crontab to run `ShellScripts` for the above scripts to run regaulary, depending on the needed intervall:
_make sure you change the path to node to yours in the scripts and make them executalbe with `chmod u+x script.sh`_

`0 */6 * * * /home/pi/projects/plant-probe/sensors.sh` (e.g. every 6th hours at minute 0 => 4 times a day)
`15,45 */1 * * * /home/pi/projects/plant-probe/syncdb.sh` (sync hourly two times)


